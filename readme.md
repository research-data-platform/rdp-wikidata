# Research Data Platform Wikidata Integration Module

## What it does

Queries the public collaborative knowledge base 
[Wikidata](https://wikidata.org) for items describing scholarly articles 
that are registered in an instance of the 
[Research Data Platform](https://gitlab.gwdg.de/research-data-platform)

* Checks for each publication item registered through the "Literature" 
module of the Research Data Platform whether a Wikidata ID can be 
assigned based on PubMed ID or DOI
* If Wikidata ID can be assigned, it is stored as an "External ID" object
* If Wikidata ID is available, it is added to the literature module's 
publication display page using the Drupal hook 
`hook_rdp_publication_display_table_alter`

## Requirements

Since this is a Drupal module, a working base instance 
of [Drupal](https://drupal.org/) 7.x including all subsequent depencies is required. 
For third party library installation, [Composer](https://getcomposer.org/) 
must be installed.

This Drupal module requires the following custom Drupal modules 
installed and enabled to work:
* [RDP Commons](https://gitlab.gwdg.de/research-data-platform/sfb-commons)
* [RDP Literature](https://gitlab.gwdg.de/research-data-platform/sfb-literature)
* RDP External ID (part of RDP Literature module distribution 
since version `7.x-1.2`)

## Installation

1. Make sure all the dependencies are installed/copied to the Drupal modules 
directory.
2. Run `composer install` in RDP_Wikidata module folder
3. Go to the Drupal modules administration page of your Drupal instance 
(`$BASE_PATH/admin/modules`) to install and enable the module.

## Libraries

* [PHP Wikidata API client](https://github.com/freearhey/wikidata)
 by Aleksandr Statciuk, MIT license